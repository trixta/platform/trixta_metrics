defmodule TrixtaMetricsMetricsSupervisorTest do
  use ExUnit.Case
  doctest TrixtaMetrics.MetricsSupervisor

  alias TrixtaMetrics.{MetricServer, MetricsSupervisor}

  setup do
    id = UUID.uuid1()
    id1 = UUID.uuid1()

    metric = %TrixtaMetrics.Metric{
      id: id,
      name: "Genèsis",
      description: "The Genèsis Metric",
      current_state: :initialising
    }

    child_spec = %{
      id: MetricServer,
      start: {MetricServer, :start_link, [metric]},
      restart: :transient
    }

    {:ok, pid} = start_supervised(MetricServer, child_spec)

    metric1 = %TrixtaMetrics.Metric{
      id: id1,
      name: "Genèsis One",
      description: "The Genèsis One Metric",
      current_state: :initialising
    }

    child_spec1 = %{
      id: MetricServer1,
      start: {MetricServer, :start_link, [metric1]},
      restart: :transient
    }

    {:ok, pid1} = start_supervised(MetricServer, child_spec1)

    [
      metric: metric,
      metric_id: metric.id,
      pid: pid,
      metric1: metric1,
      metric1_id: metric1.id,
      pid1: pid1
    ]
  end

  test "metrics supervisor restarts metric on server crash", context do
    pid = context[:pid]
    ref = Process.monitor(pid)
    Process.exit(pid, :kill)

    receive do
      {:DOWN, ^ref, :process, ^pid, :killed} ->
        :timer.sleep(1)
        assert is_pid(TrixtaMetrics.MetricServer.metric_pid(context[:metric_id]))
    after
      1000 ->
        raise :timeout
    end
  end

  test "can list active metrics", _context do
    metric_ids_list = MetricsSupervisor.list_metric_ids()
    # assert metric_ids_list == []
    assert length(metric_ids_list) >= 2
  end

  test "can list inactive metrics", _context do
    active_metrics_list = MetricsSupervisor.list_metric_ids()
    assert TrixtaMetrics.stop_metric(hd(active_metrics_list)) == :ok
    inactive_metrics_list = MetricsSupervisor.list_metric_ids(:inactive)
    all_metrics_list = MetricsSupervisor.list_metric_ids(:all)
    assert length(inactive_metrics_list) < length(all_metrics_list)
  end

  test "can list all metrics with details", _context do
    metrics_list = MetricsSupervisor.list_metric_summaries()
    assert length(metrics_list) > 1
    # assert metrics_list == []
    assert hd(metrics_list).name == "Genèsis One"
  end
end
