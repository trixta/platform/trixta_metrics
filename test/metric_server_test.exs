defmodule TrixtaMetricsMetricServerTest do
  use ExUnit.Case
  doctest TrixtaMetrics.MetricServer

  alias TrixtaMetrics.MetricServer

  setup do
    id = UUID.uuid1()

    metric = %TrixtaMetrics.Metric{
      id: id,
      name: "Genèsis",
      description: "The Genèsis Metric",
      current_state: :initialising
    }

    child_spec = %{
      id: MetricServer,
      start: {MetricServer, :start_link, [metric]},
      restart: :transient
    }

    {:ok, pid} = start_supervised(MetricServer, child_spec)

    [metric: metric, id: id, pid: pid]
  end

  test "can get metric summary", %{id: id} do
    summary = MetricServer.summarize(id)

    assert summary.id != nil
    assert summary.name == "Genèsis"
    assert summary.description == "The Genèsis Metric"
    assert summary.current_state == :initialising
  end

  test "can update Metric given valid complete new state", %{metric: metric} do
    new_state = %{
      id: metric.id,
      name: "Genèsis Two",
      description: "The Genèsis Two Metric",
      current_state: :active
    }

    {:ok, updated_metric} = MetricServer.update_state(metric.id, new_state)

    assert updated_metric.id == metric.id
    assert updated_metric.name == "Genèsis Two"
    assert updated_metric.description == "The Genèsis Two Metric"
    assert updated_metric.current_state == :active
  end

  test "can update Metric given valid partial new state", %{metric: metric} do
    new_state = %{
      name: "Genèsis Three",
      current_state: :waiting
    }

    {:ok, updated_metric} = MetricServer.update_state(metric.id, new_state)

    assert updated_metric.id == metric.id
    assert updated_metric.name == "Genèsis Three"
    assert updated_metric.description == "The Genèsis Metric"
    assert updated_metric.current_state == :waiting
  end
end
