defmodule TrixtaMetricsMetricTest do
  use ExUnit.Case
  doctest TrixtaMetrics.Metric

  alias TrixtaMetrics.Metric

  setup do
    id = UUID.uuid1()

    init_state = %TrixtaMetrics.Metric{
      id: id,
      name: "Genèsis",
      description: "The Genèsis Metric",
      current_state: :initialising
    }

    metric = Metric.new(init_state)

    [metric: metric]
  end

  test "new_metric returns the intial state" do
    id = UUID.uuid1()

    init_state = %TrixtaMetrics.Metric{
      id: id,
      name: "Genèsis",
      description: "The Genèsis Metric",
      current_state: :initialising
    }

    metric = Metric.new(init_state)

    assert metric.id == id
    assert metric.name == "Genèsis"
    assert metric.description == "The Genèsis Metric"
    assert metric.current_state == :initialising
  end

  test "new_metric returns the intial state with default values" do
    id = UUID.uuid1()

    init_state = %TrixtaMetrics.Metric{
      id: id
    }

    metric = Metric.new(init_state)

    assert metric.id != nil
    assert metric.name =~ ~r/^[a-z]+-[a-z]+-[0-9]+/
    assert metric.description =~ ~r/.+/
    assert metric.current_state == :initialising
  end

  test "can get metric summary", context do
    summary = Metric.summarize(context[:metric])

    assert summary.id != nil
    assert summary.name == "Genèsis"
    assert summary.description == "The Genèsis Metric"
    assert summary.current_state == :initialising
  end

  test "can update Metric given valid complete new state", %{metric: metric} do
    new_state = %{
      id: metric.id,
      name: "Genèsis Two",
      description: "The Genèsis Two Metric",
      current_state: :active
    }

    {:ok, updated_metric} = Metric.update_state(metric, new_state)

    assert updated_metric.id == metric.id
    assert updated_metric.name == "Genèsis Two"
    assert updated_metric.description == "The Genèsis Two Metric"
    assert updated_metric.current_state == :active
  end

  test "can update Metric given valid partial new state", %{metric: metric} do
    new_state = %{
      name: "Genèsis Three",
      current_state: :waiting
    }

    {:ok, updated_metric} = Metric.update_state(metric, new_state)

    assert updated_metric.id == metric.id
    assert updated_metric.name == "Genèsis Three"
    assert updated_metric.description == "The Genèsis Metric"
    assert updated_metric.current_state == :waiting
  end
end
