let
  unstable = import (fetchTarball https://nixos.org/channels/nixos-unstable/nixexprs.tar.xz) { };
  # unstable = import <nixos-unstable> {};
in
  with import <nixpkgs> {};
  stdenv.mkDerivation {
    name = "env";
    buildInputs = [
      unstable.gitFull
      unstable.beam.packages.erlangR24.elixir
      unstable.nodejs
      unstable.inotify-tools
      unstable.sops
      unstable.kubectl
      unstable.kops
      unstable.kubernetes-helm
      unstable.helmfile
      unstable.terraform
    ];
  }
