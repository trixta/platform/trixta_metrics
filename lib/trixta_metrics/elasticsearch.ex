defmodule TrixtaMetrics.ElasticsearchCluster do
  use Elasticsearch.Cluster, otp_app: :trixta_metrics

  # This is now set in app config and is no longer needed.

  # def init(config) do
  #   config =
  #     config
  #     |> Map.put(:url, System.get_env("ELASTICSEARCH_DOMAIN"))

  #   {:ok, config}
  # end
end
