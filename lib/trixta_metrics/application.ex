defmodule TrixtaMetrics.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    children = [
      # Starts a worker by calling: TrixtaMetrics.Worker.start_link(arg)
      # {TrixtaMetrics.Worker, arg},
      TrixtaMetrics.MetricsSupervisor,
      TrixtaMetrics.ElasticsearchCluster
    ]

    TrixtaStorage.open_table(:trixta_metrics, :metrics_table)

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: TrixtaMetrics.Supervisor]
    ret = Supervisor.start_link(children, opts)

    # load any existing metrics
    TrixtaStorage.traverse(:metrics_table, fn {_metric_id, metric} ->
      case TrixtaMetrics.start_metric(metric) do
        {:error, {:already_started, _pid}} -> :continue
        {:error, {:unknown_error}, _} -> :continue
        {:ok, _pid, _metric} -> :continue
      end
    end)

    ret
  end
end
