defmodule TrixtaMetrics.MetricsSupervisor do
  @moduledoc """
  A supervisor that starts `MetricServer` processes dynamically.
  """

  use DynamicSupervisor

  require Logger

  alias TrixtaMetrics.MetricServer

  def start_link(_arg) do
    DynamicSupervisor.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end

  @doc """
  Starts a `MetricServer` process given a Metric struct and supervises it.
  """
  def start_metric(metric) do
    child_spec = %{
      id: MetricServer,
      start: {MetricServer, :start_link, [metric]},
      restart: :transient
    }

    DynamicSupervisor.start_child(__MODULE__, child_spec)
  end

  @doc """
  Stop the `MetricServer` process normally. It CAN be restarted.
  """
  def stop_metric(metric_id) do
    with child_pid when not is_nil(child_pid) <- MetricServer.metric_pid(metric_id) do
      DynamicSupervisor.terminate_child(__MODULE__, child_pid)
      Logger.info("Stopped metric server process with ID '#{metric_id}'.")
    end
  end

  @doc """
  Terminates the `MetricServer` process AND state normally. It CAN'T be restarted.
  """
  def terminate_metric(metric_id) do
    with child_pid when not is_nil(child_pid) <- MetricServer.metric_pid(metric_id) do
      DynamicSupervisor.terminate_child(__MODULE__, child_pid)
      Logger.info("Stopped metric server process with ID '#{metric_id}'.")
    end

    TrixtaStorage.delete(:metrics_table, metric_id)
    Logger.info("TERMINATING metric server process and state with ID '#{metric_id}'...")
  end

  @doc """
  List Metrics
  """
  def list_metric_ids() do
    list_metric_ids(:processes)
  end

  def list_metric_ids(:processes) do
    :global.registered_names()
    |> Enum.filter(fn {module, name} -> module == TrixtaMetrics.MetricRegistry end)
    |> Enum.reduce([], fn {_, name}, names -> [name | names] end)
    |> Enum.sort()
  end

  def list_metric_ids(:all) do
    TrixtaStorage.traverse(:metrics_table, fn {metric_id, _metric} ->
      {:continue, metric_id}
    end)
    |> Enum.sort()
  end

  def list_metric_ids(:inactive) do
    MapSet.difference(MapSet.new(list_metric_ids(:all)), MapSet.new(list_metric_ids(:processes)))
    |> MapSet.to_list()
  end

  def list_metric_ids(:difference) do
    MapSet.difference(MapSet.new(list_metric_ids(:all)), MapSet.new(list_metric_ids(:processes)))
    |> MapSet.to_list()
  end

  def list_metric_ids(:myers_difference) do
    List.myers_difference(list_metric_ids(:processes), list_metric_ids(:all))
  end

  @doc """
  List Metric summaries
  """
  def list_metric_summaries() do
    TrixtaStorage.traverse(:metrics_table, fn {_metric_id, metric} ->
      {:continue, metric}
    end)
  end
end
