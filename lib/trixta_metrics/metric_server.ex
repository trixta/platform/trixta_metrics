defmodule TrixtaMetrics.MetricServer do
  @moduledoc """
  A metric server process that holds a `Metric` struct as its state.
  """

  use GenServer

  require Logger

  alias TrixtaMetrics.Metric

  @backup_interval :timer.minutes(5)

  # Client (Public) Interface

  @doc """
  Spawns a new metric server process registered under the given `metric_id`.
  """
  def start_link(metric) do
    GenServer.start_link(
      __MODULE__,
      {metric},
      name: via_tuple(metric.id)
    )
  end

  def summarize(metric_id) do
    GenServer.call(via_tuple(metric_id), :summary)
  end

  def update_state(metric_id, %{} = new_state) do
    GenServer.call(via_tuple(metric_id), {:update_state, new_state})
  end

  @doc """
  Returns a tuple used to register and lookup a metric server process by metric_id.
  """
  def via_tuple(metric_id) when is_pid(metric_id) do
    metric_id
  end

  def via_tuple(metric_id) do
    {:via, :global, {TrixtaMetrics.MetricRegistry, metric_id}}
  end

  @doc """
  Returns the `pid` of the metric server process registered under the given `metric_id`, or `nil` if no process is registered.
  """
  def metric_pid(metric_id) do
    metric_id
    |> via_tuple()
    |> GenServer.whereis()
  end

  # Server Callbacks

  def init(metric) do
    {metric} = metric
    metric_id = metric.id

    metric =
      case TrixtaStorage.lookup(:metrics_table, metric.id) do
        [] ->
          new_metric = Metric.new(metric)
          TrixtaStorage.save(:metrics_table, metric.id, new_metric)
          new_metric

        [{^metric_id, metric}] ->
          Logger.info("Metric with ID '#{metric_id}' already exisits. Restarting...")
          metric
      end

    Logger.info("Spawned metric server process for metric ID '#{metric.id}'.")

    schedule_backup()
    {:ok, metric}
  end

  def handle_call(:summary, _from, metric) do
    {:reply, Metric.summarize(metric), metric}
  end

  def handle_call({:update_state, new_state}, _from, metric) do
    case Metric.update_state(metric, new_state) do
      {:ok, new_metric} ->
        TrixtaStorage.save(:metrics_table, my_metric_id(), new_metric)
        {:reply, {:ok, Metric.summarize(new_metric)}, new_metric}

      {:error, messages} ->
        {:reply, {:error, messages}}
    end
  end

  def handle_info(:backup, metric) do
    # TODO: Do backup of the state here
    # Logger.info("State for metric server process with metric ID '#{metric.id}' would be backed up now.")

    schedule_backup()
    {:noreply, metric}
  end

  defp my_metric_id do
    ids = :global.registered_names()

    id =
      Enum.find_value(ids, fn id ->
        if :global.whereis_name(id) == self(), do: id
      end)

    {_, id} = id
    id
  end

  defp schedule_backup() do
    Process.send_after(self(), :backup, @backup_interval)
  end
end
