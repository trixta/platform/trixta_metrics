defmodule TrixtaMetrics.Metric do
  defstruct(
    id: nil,
    name: nil,
    description: "Shiny new Metric.",
    current_state: :initialising
  )

  def summarize(metric) do
    %{
      id: metric.id,
      name: metric.name,
      description: metric.description,
      current_state: metric.current_state
    }
  end

  def new(%TrixtaMetrics.Metric{id: nil}) do
    raise ArgumentError, message: "the argument ID is invalid"
  end

  def new(metric = %TrixtaMetrics.Metric{}) do
    metric =
      if metric.name do
        metric
      else
        %{metric | name: TrixtaNames.generate()}
      end

    metric
  end

  def update_state(metric, new_state) do
    new_state =
      new_state
      |> Map.new(fn {k, v} ->
        case is_atom(k) do
          false -> {String.to_existing_atom(k), v}
          true -> {k, v}
        end
      end)

    validate(Map.merge(metric, new_state))
  end

  # TODO: Can validate any whole or partial new space state
  # **Validation needs to happen here**
  defp validate(new_state) do
    {:ok, new_state}
    # valid? = {:ok, new_state}

    # case valid? do
    #   {:ok, new_state} ->
    #     {:ok, new_state}

    #   {:error, messages} ->
    #     {:error, messages}
    # end
  end
end
