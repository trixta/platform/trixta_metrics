defmodule TrixtaMetrics do
  def start_metric() do
    start_metric(%TrixtaMetrics.Metric{id: UUID.uuid1()})
  end

  def start_metric(%TrixtaMetrics.Metric{id: nil} = metric) do
    metric = Map.put(metric, :id, UUID.uuid1())
    start_metric(metric)
  end

  def start_metric(%TrixtaMetrics.Metric{} = metric) do
    case TrixtaMetrics.MetricsSupervisor.start_metric(metric) do
      {:ok, pid} -> {:ok, pid, TrixtaMetrics.MetricServer.summarize(pid)}
      {:error, _} = error -> error
      _ -> {:error, {:unknown_error}, metric: metric}
    end
  end

  # If `metric` is a plain map then atomize keys and structify
  def start_metric(%{} = metric) do
    metric =
      metric
      |> Map.new(fn {k, v} ->
        case is_atom(k) do
          false -> {String.to_existing_atom(k), v}
          true -> {k, v}
        end
      end)

    start_metric(struct(TrixtaMetrics.Metric, metric))
  end

  def start_metric(metric_id) do
    start_metric(%TrixtaMetrics.Metric{id: metric_id})
  end

  def stop_metric(metric_id) do
    TrixtaMetrics.MetricsSupervisor.stop_metric(metric_id)
  end

  def terminate_metric(metric_id) do
    TrixtaMetrics.MetricsSupervisor.terminate_metric(metric_id)
  end

  def list() do
    case Elasticsearch.get(TrixtaMetrics.ElasticsearchCluster, "/trixta_spaces-*/_search?pretty") do
      {:ok, result} ->
        result
    end
  end

  def list(index_name) do
    case Elasticsearch.get(TrixtaMetrics.ElasticsearchCluster, "/#{index_name}/_search?pretty") do
      {:ok, result} ->
        result
    end
  end

  def dashboard_list() do
    case Elasticsearch.post(
           TrixtaMetrics.ElasticsearchCluster,
           "/.trixta_analytics/_search",
           %{
             "size" => 500,
             "query" => %{
               "match" => %{
                 type: "dashboard"
               }
             }
           }
         ) do
      {:ok, result} ->
        result
    end
  end

  def dashboard_get(dashboard_id) do
    id = Map.get(dashboard_id, "dashboard_id")

    case Elasticsearch.get(TrixtaMetrics.ElasticsearchCluster, "/.trixta_analytics/doc/#{id}") do
      {:ok, result} ->
        result
    end
  end

  def dashboard_create_or_update(%{"dashboard_id" => dashboard_id, "data" => data}) do
    case Elasticsearch.post(
           TrixtaMetrics.ElasticsearchCluster,
           "/.trixta_analytics/doc/#{dashboard_id}",
           data
         ) do
      {:ok, result} ->
        result
    end
  end

  def dashboard_delete(%{"dashboard_id" => dashboard_id}) do
    case Elasticsearch.delete(
           TrixtaMetrics.ElasticsearchCluster,
           "/.trixta_analytics/doc/#{dashboard_id}"
         ) do
      {:ok, result} ->
        result
    end
  end

  def raw_search(data) do
    query = Map.get(data, "data")

    case Elasticsearch.post(
           TrixtaMetrics.ElasticsearchCluster,
           "/trixta_spaces-*/_search?pretty",
           query
         ) do
      {:ok, result} ->
        {:ok, result}

      {:error, reason} ->
        {:error, Map.get(reason, :reason)}
    end
  end

  def get_last_25_transactions(address) do
    query = build_latest_transactions_query("eth_transaction_log", address)

    case Elasticsearch.post(
           TrixtaMetrics.ElasticsearchCluster,
           "/trixta_spaces-*/_search?pretty",
           query
         ) do
      {:ok, result} ->
        result["hits"]["hits"]

      _ ->
        []
    end
  end

  def get_last_25_erc20_token_transactions(address) do
    query = build_latest_transactions_query("eth_erc20_token_event", address)

    case Elasticsearch.post(
           TrixtaMetrics.ElasticsearchCluster,
           "/trixta_spaces-*/_search?pretty",
           query
         ) do
      {:ok, result} ->
        result["hits"]["hits"]

      _ ->
        []
    end
  end

  def delete_search_data(data) do
    query = Map.get(data, "data")

    case Elasticsearch.post(
           TrixtaMetrics.ElasticsearchCluster,
           "/trixta_spaces-*/_delete_by_query",
           query
         ) do
      {:ok, result} ->
        {:ok, result}
    end
  end

  def delete_all_search_data() do
    case Elasticsearch.delete(TrixtaMetrics.ElasticsearchCluster, "/trixta_spaces-*/") do
      {:ok, result} ->
        {:ok, result}
    end
  end

  def multi_search(%{"search_data" => search_data}) do
    case Elasticsearch.post(TrixtaMetrics.ElasticsearchCluster, "/_all/_msearch", search_data) do
      {:ok, result} ->
        {:ok, result}
    end
  end

  def get_stats() do
    case Elasticsearch.get(TrixtaMetrics.ElasticsearchCluster, "/trixta_spaces-*/_stats") do
      {:ok, result} ->
        result
    end
  end

  def get_cluster_stats() do
    case Elasticsearch.get(TrixtaMetrics.ElasticsearchCluster, "/_cluster/stats?human&pretty") do
      {:ok, result} ->
        result
    end
  end

  def create_index_for_data_sets() do
    create_index_for_data_set("contract-logs", "contract-logs")
    create_index_for_data_set("transaction-logs", "transaction-logs")
  end

  def create_index_for_data_set(file_ref, index_name) do
    file_path =
      :code.priv_dir(:trixta_metrics)
      |> Path.join("elastic_search_templates/#{file_ref}.json")
      |> Path.expand()

    case Elasticsearch.Index.create_from_file(
           TrixtaMetrics.ElasticsearchCluster,
           index_name,
           file_path
         ) do
      :ok ->
        {:ok, "index created"}
    end
  end

  # String index_name, Array[Map] items to be uploaded
  def bulk_upload(index_name, items) do
    # Stream.map |> #{document}\n" |> Enum.join
    elastic_search_data =
      items
      |> Enum.map_join("\n", &"{\"index\":{}}\n#{Poison.encode!(&1)}")
      |> Kernel.<>("\n")

    case Elasticsearch.put(
           TrixtaMetrics.ElasticsearchCluster,
           "/#{index_name}/_doc/_bulk",
           elastic_search_data
         ) do
      {:ok, results} ->
        results
    end
  end

  def disable_elastic_search_index_refresh(index_name) do
    disable_instruction = %{
      "index" => %{
        "refresh_interval" => "-1"
      }
    }

    case Elasticsearch.put(
           TrixtaMetrics.ElasticsearchCluster,
           "/#{index_name}/_settings",
           disable_instruction
         ) do
      {:ok, results} ->
        results
    end
  end

  def enable_elastic_search_index_refresh(index_name) do
    enable_instruction = %{
      "index" => %{
        "refresh_interval" => nil
      }
    }

    case Elasticsearch.put(
           TrixtaMetrics.ElasticsearchCluster,
           "/#{index_name}/_settings",
           enable_instruction
         ) do
      {:ok, results} ->
        results
    end
  end

  defp build_latest_transactions_query(cat, address) do
    %{
      "_source" => [
        "to",
        "from",
        "value",
        "ts",
        "hash"
      ],
      "size" => 25,
      "query" => %{
        "bool" => %{
          "filter" => %{
            "bool" => %{
              "must" => [
                %{
                  "term" => %{
                    "cat" => cat
                  }
                },
                %{
                  "term" => %{
                    "to" => address
                  }
                }
              ]
            }
          }
        }
      }
    }
  end
end
